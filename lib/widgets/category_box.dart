import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:leaso/models/category_list_data.dart';

class CategoryBox extends StatelessWidget {

  String categoryBoxText;
  Color color;
  CategoryBox({this.color, this.categoryBoxText});
  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      position: DecorationPosition.background,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.blue),
            color: color,
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        child: Container(padding: EdgeInsets.all(10.0) ,
          child: Center(
            child: Text(
              categoryBoxText,
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue),
              textAlign: TextAlign.center,
            ),
          ),
        ));
  }
}
