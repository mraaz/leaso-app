import 'package:flutter/material.dart';
import 'package:leaso/models/category_list_data.dart';
import 'package:leaso/provider/bottom_navigation_bar_provider.dart';
import 'package:leaso/screens/home_screen.dart';
import 'package:leaso/screens/insert_screen.dart';
import 'package:leaso/screens/login_screen.dart';
import 'package:leaso/screens/profile_screen.dart';
import 'package:leaso/screens/messages_screen.dart';
import 'package:provider/provider.dart';

import 'provider/bottom_navigation_bar_provider.dart';
import 'provider/bottom_navigation_bar_provider.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<BottomNavigationBarProvider>(
          builder: (BuildContext context) => BottomNavigationBarProvider(),
        ),
        ChangeNotifierProvider<CategoryListData>(
          builder: (BuildContext context) => CategoryListData(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primaryColor: Colors.orangeAccent),
        home: LoginScreen(),
      ),
    );
  }
}

class BottomNavigationBarExample extends StatefulWidget {
  @override
  _BottomNavigationBarExampleState createState() =>
      _BottomNavigationBarExampleState();
}

class _BottomNavigationBarExampleState
    extends State<BottomNavigationBarExample> {
  var currentTab = [
    HomeScreen(),
    InsertScreen(),
    MessagesScreen(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<BottomNavigationBarProvider>(context);
    return Scaffold(
      body: currentTab[provider.currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.black,
        currentIndex: provider.currentIndex,
        onTap: (index) {
          provider.currentIndex = index;
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Colors.black),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add, color: Colors.black),
            title: Text('Inserieren'),
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.message,
                color: Colors.black,
              ),
              title: Text('Nachrichten')),
          BottomNavigationBarItem(
              icon: Icon(Icons.person, color: Colors.black),
              title: Text('Einstellungen'))
        ],
      ),
    );
  }
}
