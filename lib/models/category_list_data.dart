import 'package:flutter/material.dart';

class CategoryItem {
  String category;

  CategoryItem({this.category});
}

class CategoryListData with ChangeNotifier {
  List<CategoryItem> _categorieslist = [
    CategoryItem(category: 'Elektronik'),
    CategoryItem(category: 'Fahrzeuge'),
    CategoryItem(category: 'Boote & Yachten'),
    CategoryItem(category: 'Personal'),
    CategoryItem(category: 'Sport & Freizeit'),
    CategoryItem(category: 'Baumaschinen'),
    CategoryItem(category: 'Wohnmobile'),
    CategoryItem(category: 'Baby & Kind'),
  ];

  List<CategoryItem> get categorieslist {
    return [..._categorieslist];
  }
}
