import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:leaso/widgets/category_box.dart';
import 'package:provider/provider.dart';
import 'package:leaso/models/category_list_data.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var categoryProvider = Provider.of<CategoryListData>(context);
    var categoryList = categoryProvider.categorieslist;
    return Scaffold(
      appBar: PreferredSize(
        child: Padding(
          padding: EdgeInsets.only(top: 40.0, left: 10.0, right: 10.0),
          child: Card(
            elevation: 6.0,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              child: TextField(
                style: TextStyle(
                  fontSize: 15.0,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.white,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.white,
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  hintText: "Search..",
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.black,
                  ),
                  hintStyle: TextStyle(
                    fontSize: 15.0,
                    color: Colors.black,
                  ),
                ),
                maxLines: 1,
              ),
            ),
          ),
        ),
        preferredSize: Size(
          MediaQuery.of(context).size.width,
          60.0,
        ),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 20.0),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Categories",
                  style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                FlatButton(
                  child: Text(
                    "See all",
                    style: TextStyle(
//                      fontSize: 22,
//                      fontWeight: FontWeight.w800,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  onPressed: () {
                    // Navigator.of(context).push(
                    //    MaterialPageRoute(
                    // builder: (BuildContext context) {
                    // return Trending();
                    //},
                    //),
                    //);
                  },
                ),
              ],
            ),

            Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              height: 50.0,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                      child: CategoryBox(categoryBoxText: categoryList[1].category, color: Colors.white),
                      width: 100.0,
                    ),
                  ),
                  SizedBox(width: 5.0),
                  Container(
                    child: CategoryBox(categoryBoxText: categoryList[2].category, color: Colors.white),
                    width: 100.0,
                  ),
                  SizedBox(width: 5.0),
                  Container(
                    child: CategoryBox(categoryBoxText: categoryList[3].category, color: Colors.white),
                    width: 100.0,
                  ),
                  SizedBox(width: 5.0),
                  Container(
                    child: CategoryBox(categoryBoxText: categoryList[4].category, color: Colors.white),
                    width: 100.0,
                  ),
                  SizedBox(width: 5.0),
                  Container(
                    child: CategoryBox(categoryBoxText: categoryList[5].category, color: Colors.white),
                    width: 120.0,
                  ),
                ],
              ),
            ),

            SizedBox(height: 30.0),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const Text(
                  "In deiner Nähe",
                  style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                FlatButton(
                  child: Text(
                    "See all",
                    style: TextStyle(
                      //                    fontSize: 22,
//                      fontWeight: FontWeight.w800,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  onPressed: () {
//                    Navigator.of(context).push(
//                      MaterialPageRoute(
//                        builder: (BuildContext context){
//                          return DishesScreen();
//                        },
//                      ),
//                    );
                  },
                ),
              ],
            ),

            SizedBox(height: 10.0),

            //Horizontal List here

            SizedBox(height: 30.0),
          ],
        ),
      ),
    );
  }
}


