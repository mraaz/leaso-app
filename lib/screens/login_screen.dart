import 'package:flutter/material.dart';
import 'package:leaso/widgets/login_screen_widget.dart';



class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  LoginScreenWidget(
        backgroundColor1: Color(0xFF444152),
        backgroundColor2: Color(0xFF6f6c7d),
        highlightColor: Color(0xfff65aa3),
        foregroundColor: Colors.white,
        logo: new AssetImage("assets/images/full-bloom.png"),
      ),
    );
  }
}
